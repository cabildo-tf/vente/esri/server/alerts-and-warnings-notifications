import 'dotenv/config'
import 'reflect-metadata'
import { createConnection } from 'typeorm'
import App from './app'
import config from './ormconfig'
import validateEnv from './utils/validateEnv'
import ItemsController from './interfaces/itemcontroller.template'
import Issue from './issues/issues.entity'
import Alert from './alerts/alerts.entity'
import AlertDTO from './alerts/alerts.dto'
import IssueDTO from './issues/issues.dto'
import DevicesController from './devices/devices.controller'
import Logger from './logger'
import AlertsSubscribeController from './alerts/alerts.subscribe'
import IssuesSubscribeController from './issues/issues.subscribe'
import RestrictionsController from './restrictions/restrictions.controller'

validateEnv()
;(async () => {
  try {
    await createConnection(config)
  } catch (error) {
    Logger.error('Error while connecting to the database', error)
    return error
  }
  const app = new App(
    [
      new ItemsController<Alert, AlertDTO>('/alerts', Alert, AlertDTO),
      new ItemsController<Issue, IssueDTO>('/issues', Issue, IssueDTO),
      new DevicesController(),
      new RestrictionsController(),
    ],
    [
      new AlertsSubscribeController('new-alert'),
      new IssuesSubscribeController('new-issue'),
    ]
  )

  app.listen()
})()

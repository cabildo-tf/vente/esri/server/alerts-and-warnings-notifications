import GroupService from '../groups/groups.service'
import GroupTemplateService from '../groupstemplate/groupstemplate.service'
import Logger from '../logger'
import User from '../users/users.entity'
import UserService from '../users/users.service'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const axios = require('axios').default

class AuthenticationService {
  private static instance: AuthenticationService
  private userService = UserService.getInstance()
  private groupService = GroupService.getInstance()
  private groupTemplateService = GroupTemplateService.getInstance()

  static getInstance(): AuthenticationService {
    if (!AuthenticationService.instance) {
      AuthenticationService.instance = new AuthenticationService()
    }

    return AuthenticationService.instance
  }

  async validateToken(token: string): Promise<User> {
    console.log('Entro en validate token')
    const arcgis_user = await this.getArcGISUser(token)
    if (!arcgis_user) {
      console.log('Entro NO arcgis_user')
      return null
    }
    const db_user = await this.userService.findUserById(arcgis_user.id)
    if (!db_user) {
      console.log('Prueba console log en if db_user')
      arcgis_user.groupstemplate = await this.groupTemplateService.findGroupsTemplateByUsername(arcgis_user.username)
      console.log('Username: ' + arcgis_user.username)
      console.log('Muestro array, Size: ' + arcgis_user.groupstemplate.length  + 'Y valores: ' + arcgis_user.groupstemplate)
      if (arcgis_user.groupstemplate.length > 0) {
        console.log('Entro en if length mayor cero')
        for(let i = 0 ; i < arcgis_user.groupstemplate.length ; i++) {
          console.log(arcgis_user.groupstemplate[i].groupId)
        }
        let groupIds = []
        for(let i = 0 ; i < arcgis_user.groupstemplate.length ; i++) {
          groupIds.push(arcgis_user.groupstemplate[i].groupId)
        }
        arcgis_user.groups = await this.groupService.findGroupsByIds(groupIds)
      } else {
        console.log('Entro en if length menor cero')
        arcgis_user.groups = await this.groupService.getDefaultGroups()
      }
      this.userService.createUser(arcgis_user)
    }
    console.log('Salgo de validate token, el arcgis user es: ' + arcgis_user)
    return arcgis_user
  }

  async getArcGISUser(token: string): Promise<User> {
    console.log('En getArcgisUser muestro el token: ' + token)
    const portal_authorize_url: string = process.env.PORTAL_AUTHORIZE_URL
    console.log('muestro portal_authorize_url: ' + portal_authorize_url)
    try {
      console.log('Entro al Try')
      const response = await axios.get(portal_authorize_url, {
        params: {
          f: 'pjson',
          token: token,
        },
      })
      console.log('Antes del if,  response es: ' + response)
      if (!response.data?.error) {
        const user = new User()
        user.id = response.data.user?.id
        user.username = response.data.user?.username
        return user
      } else {
        console.log('Error en la respuesta del Portal:', response.data.error)
      }
    } catch (error) {
      Logger.warn('Error en la petición al Portal', error)
    }
  }
}

export default AuthenticationService

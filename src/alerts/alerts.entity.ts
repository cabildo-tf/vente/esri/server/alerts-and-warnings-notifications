import { ViewEntity, ViewColumn } from 'typeorm'
import Item from '../interfaces/item.interface'

@ViewEntity({
  expression: `
  SELECT a.objectid, a.globalid, a.codigo, a.causa AS causa_id, c.name AS causa_nombre, a.situacion,
    a.fecha_inicio, a.fecha_fin,
    a.observaciones, gu."groupIds", gu."userIds", aat.attachmentid,
       case when ((now() BETWEEN a.fecha_inicio AND a.fecha_fin) OR a.fecha_fin is null) then true else false end as activa
 FROM ( SELECT aa.*,
            row_number() OVER (PARTITION BY aa.codigo ORDER BY aa.fecha_fin DESC NULLS LAST, aa.objectid DESC NULLS LAST) AS rn
           FROM alerta aa) a
		   LEFT JOIN vente.alerta__attach aat ON a.globalid = aat.rel_globalid
           LEFT OUTER JOIN (SELECT * FROM app.get_domain_values('alerta_causa')) c ON a.causa = c.code,
           (SELECT array_agg(DISTINCT user_groups_group."userId") AS "userIds", array_agg(DISTINCT user_groups_group."groupId") AS "groupIds"
               FROM app.user_groups_group) gu
 WHERE a.rn = 1
 GROUP BY a.objectid, a.globalid, a.codigo, a.causa, c.name, a.situacion,
    a.fecha_inicio, a.fecha_fin,
    a.observaciones, gu."groupIds",  gu."userIds", aat.attachmentid
 ORDER BY a.fecha_inicio DESC;`,
})
class Alerts extends Item {
  @ViewColumn()
  public objectid: number

  @ViewColumn()
  public causa_id: number

  @ViewColumn()
  public causa_nombre: string

  @ViewColumn()
  public situacion: number

  @ViewColumn()
  public observaciones: string

  @ViewColumn()
  public fecha_inicio: Date

  @ViewColumn()
  public fecha_fin: Date

  @ViewColumn()
  public activa: boolean

  @ViewColumn()
  public attachmentid: number
  
}

export default Alerts
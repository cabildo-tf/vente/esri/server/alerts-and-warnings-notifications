import ItemDTO from '../interfaces/item-dto.interface'
import Alerts from './alerts.entity'

class AlertDTO extends ItemDTO {
  public causa_id: number
  public causa_nombre: string
  public situacion: number
  public observaciones: string
  public fecha_inicio: Date
  public fecha_fin: Date
  public activa: boolean
  public attachmentid: number
  public objectid: number


  fromModel(model: Alerts) {
    super.fromModel(model)
    this.causa_id = model.causa_id
    this.causa_nombre = model.causa_nombre
    this.situacion = model.situacion
    this.observaciones = model.observaciones
    this.fecha_inicio = model.fecha_inicio
    this.fecha_fin = model.fecha_fin
    this.activa = model.activa
    this.objectid = model.objectid
    this.attachmentid = model.attachmentid

  }
}

export default AlertDTO

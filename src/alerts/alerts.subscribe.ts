import { TokenMessage } from 'firebase-admin/lib/messaging/messaging-api'
import { DateTime } from 'luxon'
import SubscribeController from '../pubsub/suscribecontroller.template'
import Alert from './alerts.entity'

class AlertsSubscribeController extends SubscribeController<Alert> {
  constructor(channel: string) {
    super(channel, 'alertas', Alert)
  }

  generateMessage(_payload: Alert): TokenMessage {
    const startDate = this.datetimeToString(_payload.fecha_inicio)
    const endDate = this.datetimeToString(_payload.fecha_fin)
    const rangeDatetime = [startDate, endDate].join(' - ')

    const message: TokenMessage = {
      notification: {
        title: `Alerta ${_payload.causa_nombre}`,
        body: `Activa desde  ${rangeDatetime}`,
      },
      android: {
        notification: {
          sound: 'default',
          clickAction: 'FCM_PLUGIN_ACTIVITY',
          icon: 'fcm_push_icon',
        },
      },
      data: {
        globalid: _payload.globalid,
        type: this.type,
      },
      token: '',
    }
    return message
  }

  datetimeToString(datetime: Date): string {
    if (datetime) {
      return datetime?.toLocaleDateString('es-ES')
    }
  }
}

export default AlertsSubscribeController

import Restriction from './restrictions.entity'

class RestrictionDTO {
  public code: number
  public name: string

  fromModel(model: Restriction) {
    this.code = model.code
    this.name = model.name
  }
}

export default RestrictionDTO

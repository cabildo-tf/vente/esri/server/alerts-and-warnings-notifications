import { ViewEntity, ViewColumn } from 'typeorm'

@ViewEntity({
  expression: `
  SELECT *
  FROM app.get_domain_values('incidencia_restriccion');`,
})
class Restriction {
  @ViewColumn()
  public code: number

  @ViewColumn()
  public name: string
}

export default Restriction

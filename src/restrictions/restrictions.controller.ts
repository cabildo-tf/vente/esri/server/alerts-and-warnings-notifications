import { Router, Request, Response, NextFunction } from 'express'
import { getRepository } from 'typeorm'
import DBErrorException from '../exceptions/DBErrorException'
import Controller from '../interfaces/controller.interface'
import Logger from '../logger'
import AuthorizationMiddleware from '../middleware/authorization.middleware'
import RestrictionDTO from './restrictions.dto'
import Restriction from './restrictions.entity'

class RestrictionsController implements Controller {
  public path = '/restrictions'
  public router = Router()
  private itemRepository = getRepository(Restriction)

  constructor() {
    this.intializeRoutes()
  }

  private intializeRoutes() {
    this.router.get(
      `${this.path}`,
      AuthorizationMiddleware,
      this.getAllRestrinctions
    )
  }

  getAllRestrinctions = async (
    request: Request,
    response: Response,
    next: NextFunction
  ) => {
    try {
      const items: Restriction[] = await this.itemRepository.find()
      const itemsDTO: RestrictionDTO[] = items.map((item) => {
        return this.mapper(item)
      })
      response.send(itemsDTO)
    } catch (err) {
      Logger.error(err)
      next(new DBErrorException())
    }
  }

  mapper(model: Restriction): RestrictionDTO {
    const item = new RestrictionDTO()
    item.fromModel(model)

    return item
  }
}

export default RestrictionsController

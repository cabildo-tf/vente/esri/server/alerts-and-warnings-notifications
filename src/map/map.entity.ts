import { Column, Entity, PrimaryColumn } from 'typeorm'

@Entity()
class Map {
  @PrimaryColumn('character varying')
  public id?: string

  @Column()
  public name: string

  @Column()
  public territorio_id: number
}

export default Map

import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
class Group {
  @PrimaryGeneratedColumn()
  public id?: number

  @Column()
  public name: string

  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @Column()
  public is_default: boolean = false
}

export default Group

import { getRepository, In } from 'typeorm'
import Group from './groups.entity'

class GroupService {
  private static instance: GroupService
  private groupRepository = getRepository(Group)

  static getInstance(): GroupService {
    if (!GroupService.instance) {
      GroupService.instance = new GroupService()
    }

    return GroupService.instance
  }

  findGroupsById(id: number): Promise<Group> {
    return this.groupRepository.findOne({ where: { id: id } })
  }

  findGroupsByIds(ids: number[]): Promise<Group[]> {
    return this.groupRepository.find({ id: In(ids) })
  }

  getDefaultGroups(): Promise<Group[]> {
    return this.groupRepository.find({ where: { is_default: true } })
  }

  createGroups(group: Group): Promise<Group> {
    return this.groupRepository.save(group)
  }
}

export default GroupService

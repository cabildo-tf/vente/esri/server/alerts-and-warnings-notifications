import { getRepository, In } from 'typeorm'
import Device from './devices.entity'

class DeviceService {
  private static instance: DeviceService
  private deviceRepository = getRepository(Device)

  static getInstance(): DeviceService {
    if (!DeviceService.instance) {
      DeviceService.instance = new DeviceService()
    }

    return DeviceService.instance
  }

  save(device: Device): Promise<Device> {
    return this.deviceRepository.save(device)
  }

  findDevicesByUsers(userIds: string[]): Promise<Device[]> {
    return this.deviceRepository.find({
      where: {
        user: {
          id: In(userIds),
        },
      },
      relations: ['user'],
    })
  }

  findDevicesByUser(userId: string): Promise<Device[]> {
    return this.deviceRepository.find({
      where: {
        user: {
          id: userId,
        },
      },
      relations: ['user'],
    })
  }
}

export default DeviceService

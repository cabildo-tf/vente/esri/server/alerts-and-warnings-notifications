import { Router, Request, Response } from 'express'
import Controller from '../interfaces/controller.interface'
import Logger from '../logger'
import AuthorizationMiddleware from '../middleware/authorization.middleware'
import CreateDeviceDto from './devices.dto'
import Device from './devices.entity'
import DeviceService from './devices.service'

class DevicesController implements Controller {
  public path = '/devices'
  public router = Router()
  private deviceService = DeviceService.getInstance()

  constructor() {
    this.intializeRoutes()
  }

  private intializeRoutes() {
    this.router.post(
      `${this.path}`,
      AuthorizationMiddleware,
      this.subscribeDevice
    )
  }

  subscribeDevice = async (request: Request, response: Response) => {
    const deviceDto: CreateDeviceDto = request.body
    const device = this.mapperDtoToModel(deviceDto)
    device.user = request.user
    try {
      await this.deviceService.save(device)
      response.send({
        subscription: true,
      })
    } catch (error) {
      Logger.error(error)
    }
  }

  mapperDtoToModel(dto: CreateDeviceDto) {
    const model = dto as Device
    model.token = dto.token
    return model
  }
}

export default DevicesController

import { IsString } from 'class-validator'

class CreateDeviceDto {
  @IsString()
  public token: string
}

export default CreateDeviceDto

import { Entity, ManyToOne, Unique, PrimaryColumn } from 'typeorm'
import User from '../users/users.entity'

@Entity()
@Unique('token_user', ['token', 'user'])
class Device {
  @PrimaryColumn()
  public token: string

  @ManyToOne(() => User, (user) => user.devices)
  public user: User
}

export default Device

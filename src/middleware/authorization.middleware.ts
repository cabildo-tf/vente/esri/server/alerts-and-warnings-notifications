import { NextFunction, Request, Response } from 'express'
import AuthenticationService from '../authorization/authorization.service'

import TokenNotFoundException from '../exceptions/TokenNotFoundException'
import Logger from '../logger'
import User from '../users/users.entity'

const AuthorizationMiddleware = async (
  request: Request,
  response: Response,
  next: NextFunction
): Promise<void> => {
  const token = extractToken(request)

  console.log('En Auth Middlew, muestro token y user: ')
  console.log('Token: ' + token)
  if (!token) {
    console.log('Problema con el token: ' + token)
    next(new TokenNotFoundException(400, 'No token'))
  }

  const user: User = await AuthenticationService.getInstance().validateToken(
    token
  )

  console.log('User: ' + user)

  if (!user) {
    console.log('Problema con el user: ' + user)
    next(new TokenNotFoundException(400, 'User no exists or token no valid'))
  } else request.user = user

  next()
}

function extractToken(request: Request): string {
  const authHeader = request.headers.authorization

  if (authHeader) {
    return authHeader.split(' ')[1]
  }

  Logger.info('No existe token')
}

export default AuthorizationMiddleware

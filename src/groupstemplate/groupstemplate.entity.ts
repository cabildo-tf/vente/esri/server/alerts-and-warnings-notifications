import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
class GroupTemplate {
  @PrimaryGeneratedColumn()
  public id?: number

  @Column()
  public username: string

  @Column()
  public groupId: number

  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @Column()
  public is_supervisor: boolean = false
}

export default GroupTemplate

import { getRepository } from 'typeorm'
import GroupTemplate from './groupstemplate.entity'

class GroupTemplateService {
  private static instance: GroupTemplateService
  private groupTemplateRepository = getRepository(GroupTemplate)

  static getInstance(): GroupTemplateService {
    if (!GroupTemplateService.instance) {
      GroupTemplateService.instance = new GroupTemplateService()
    }

    return GroupTemplateService.instance
  }

  findGroupsTemplateById(id: number): Promise<GroupTemplate> {
    return this.groupTemplateRepository.findOne({ where: { id: id } })
  }

  findGroupsTemplateByUsername(username: string): Promise<GroupTemplate []> {
    return this.groupTemplateRepository.find({ where: { username: username } })
  }

  createGroupsTemplate(grouptemplate: GroupTemplate): Promise<GroupTemplate> {
    return this.groupTemplateRepository.save(grouptemplate)
  }
}

export default GroupTemplateService


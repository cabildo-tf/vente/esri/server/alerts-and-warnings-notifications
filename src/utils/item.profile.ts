import { ConfigProfile } from 'typeorm-express-query-builder/profile/config-profile'

const ItemProfile: ConfigProfile = {
  options: {
    pagination: {
      status: 'disabled',
      paginate: false,
      itemsPerPage: 1,
    },
    ordering: {
      status: 'disabled',
    },
    relations: {
      status: 'enabled',
    },
    select: {
      status: 'enabled',
    },
  },
  policy: 'skip',
}

export default ItemProfile

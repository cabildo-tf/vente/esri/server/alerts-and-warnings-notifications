function toBoolean(value?: string): boolean {
  if (!value) {
    return false
  }

  switch (value.toLocaleLowerCase()) {
    case 'true':
    case '1':
    case 'on':
    case 'yes':
      return true
    default:
      return false
  }
}

export default toBoolean

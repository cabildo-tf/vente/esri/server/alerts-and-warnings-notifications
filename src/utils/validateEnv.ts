import { cleanEnv, str, port, bool } from 'envalid'

function validateEnv() {
  cleanEnv(process.env, {
    PORT_HTTP: port(),
    URL_PREFIX: str(),
    PORTAL_AUTHORIZE_URL: str(),
    POSTGRES_HOST: str(),
    POSTGRES_PORT: port(),
    POSTGRES_USER: str(),
    POSTGRES_PASSWORD: str(),
    POSTGRES_DB: str(),
    POSTGRES_DEBUG: bool(),
  })
}

export default validateEnv

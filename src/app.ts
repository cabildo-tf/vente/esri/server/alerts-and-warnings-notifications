import express from 'express'
import * as bodyParser from 'body-parser'
import Controller from './interfaces/controller.interface'
import errorMiddleware from './middleware/error.middleware'
import PubSubService from './pubsub/pubsub.service'
import SubscribeController from './pubsub/suscribecontroller.template'
import Item from './interfaces/item.interface'
import User from './users/users.entity'
import Logger from './logger'

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    interface Request {
      user: User
    }
  }
}

class App {
  public app: express.Application
  public port: number
  pubSub: PubSubService

  constructor(
    controllers: Controller[],
    subscribers: SubscribeController<Item>[]
  ) {
    this.app = express()
    this.initializePubSubServices(subscribers)
    this.initializeMiddlewares()
    this.initializeControllers(controllers)
    this.initializeErrorHandling()
  }

  public listen() {
    this.app.listen(process.env.PORT_HTTP, () => {
      Logger.info(`App listening on the port ${process.env.PORT_HTTP}`)
    })
  }

  private initializeMiddlewares() {
    this.app.use(bodyParser.json())
  }

  private initializeControllers(controllers: Controller[]) {
    controllers.forEach((controller) => {
      this.app.use(process.env.URL_PREFIX || '/', controller.router)
    })
  }

  private initializePubSubServices(subscribers: SubscribeController<Item>[]) {
    this.pubSub = new PubSubService(subscribers)
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware)
  }
}

export default App

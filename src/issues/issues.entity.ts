import { ViewEntity, ViewColumn } from 'typeorm'
import Item from '../interfaces/item.interface'

@ViewEntity({
  expression: `WITH vente_issues AS (
    SELECT i.globalid,
       i.codigo,
       i.nombre,
       i.incidencia_prioridad_id,
       i.incidencia_tipo_accion_id,
       i.grupo_incidencia_id,
       i.subgrupo_incidencia_id,
       i.destinatario_st,
       i.restriccion_id,
       i.descripcion,
       i.territorio_id,
       i.territorio_nombre,
       i.map_id,
       i.municipio_id,
       i.gestor_id,
       i.estado_incidencia_id,
       i.created_date,
       i.created_user,
       i.last_edited_date,
       i.last_edited_user,
       i.utmx,
       i.utmy,
       i.longitud,
       i.latitud,
       i."groupIds",
       l.id AS layer_id,
       l.nombre AS layer_name
      FROM app.layer l
        CROSS JOIN LATERAL app.incidencias_by_type(l.tablename::text, l.fk_name::text) i(globalid, codigo, nombre, incidencia_prioridad_id, incidencia_tipo_accion_id, grupo_incidencia_id, subgrupo_incidencia_id, destinatario_st, restriccion_id, descripcion, territorio_id, territorio_nombre, map_id, municipio_id, gestor_id, estado_incidencia_id, created_date, created_user, last_edited_date, last_edited_user, utmx, utmy, longitud, latitud, "groupIds")
   UNION
    SELECT i.globalid,
       i.codigo,
       i.nombre,
       i.incidencia_prioridad_id,
       i.incidencia_tipo_accion_id,
       i.grupo_incidencia_id,
       i.subgrupo_incidencia_id,
       i.destinatario_st,
       i.restriccion_id,
       i.descripcion,
       i.territorio_id,
       i.territorio_nombre,
       i.map_id,
       i.municipio_id,
       i.gestor_id,
       i.estado_incidencia_id,
       i.created_date,
       i.created_user,
       i.last_edited_date,
       i.last_edited_user,
       i.utmx,
       i.utmy,
       i.longitud,
       i.latitud,
       i."groupIds",
       999 AS layer_id,
       'incidencia_libre'::character varying AS layer_name
      FROM app.incidencias_libres() i(globalid, codigo, nombre, incidencia_prioridad_id, incidencia_tipo_accion_id, grupo_incidencia_id, subgrupo_incidencia_id, destinatario_st, restriccion_id, descripcion, territorio_id, territorio_nombre, map_id, municipio_id, gestor_id, estado_incidencia_id, created_date, created_user, last_edited_date, last_edited_user, utmx, utmy, longitud, latitud, "groupIds")
   ), users_groups AS (
    SELECT user_groups_group."userId",
       array_agg(user_groups_group."groupId") AS "groupIds"
      FROM app.user_groups_group
     GROUP BY user_groups_group."userId"
   )
SELECT i.globalid,
i.codigo,
i.nombre,
i.incidencia_prioridad_id,
p.name AS incidencia_prioridad_nombre,
i.incidencia_tipo_accion_id,
i.restriccion_id,
i.grupo_incidencia_id,
i.subgrupo_incidencia_id,
i.destinatario_st,
i.descripcion,
i.territorio_id,
i.territorio_nombre,
i.map_id,
i.municipio_id,
i.gestor_id,
i.estado_incidencia_id,
i.created_date,
i.created_user,
i.last_edited_date,
i.last_edited_user,
i.utmx,
i.utmy,
i.longitud,
i.latitud,
i."groupIds",
i.layer_id,
i.layer_name,
array_agg(DISTINCT gu."userId") AS "userIds"
FROM vente_issues i
LEFT JOIN app.get_domain_values('incidencia_prioridad'::text) p(code, name) ON p.code = i.incidencia_prioridad_id,
users_groups gu
WHERE i."groupIds" && gu."groupIds"
GROUP BY i.globalid, i.codigo, i.nombre, i.incidencia_prioridad_id, p.name, i.incidencia_tipo_accion_id, i.restriccion_id, i.grupo_incidencia_id, i.subgrupo_incidencia_id, i.destinatario_st, i.descripcion, i.territorio_id, i.territorio_nombre, i.map_id, i.municipio_id, i.gestor_id, i.estado_incidencia_id, i.created_date, i.created_user, i.last_edited_date, i.last_edited_user, i.utmx, i.utmy, i.longitud, i.latitud, i."groupIds", i.layer_id, i.layer_name;`,
})
class Issues extends Item {
  @ViewColumn()
  public nombre: string

  @ViewColumn()
  public incidencia_prioridad_id: number

  @ViewColumn()
  public incidencia_prioridad_nombre: string

  @ViewColumn()
  public incidencia_tipo_accion_id: number

  @ViewColumn()
  public restriccion_id: number

  @ViewColumn()
  public grupo_incidencia_id: number

  @ViewColumn()
  public subgrupo_incidencia_id: number

  @ViewColumn()
  public destinatario_st: number

  @ViewColumn()
  public descripcion: string

  @ViewColumn()
  public territorio_id: number

  @ViewColumn()
  public territorio_nombre: string

  @ViewColumn()
  public map_id: string

  @ViewColumn()
  public municipio_id: number

  @ViewColumn()
  public gestor_id: number

  @ViewColumn()
  public estado_incidencia_id: number

  @ViewColumn()
  public created_date: Date

  @ViewColumn()
  public created_user: string

  @ViewColumn()
  public last_edited_date: Date

  @ViewColumn()
  public last_edited_user: string

  @ViewColumn()
  public utmx: number

  @ViewColumn()
  public utmy: number

  @ViewColumn()
  public longitud: number

  @ViewColumn()
  public latitud: number

  @ViewColumn()
  public layer_id: number

  @ViewColumn()
  public layer_name: string
}

export default Issues

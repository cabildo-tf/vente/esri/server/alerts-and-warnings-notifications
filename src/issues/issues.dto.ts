import ItemDTO from '../interfaces/item-dto.interface'
import Issue from './issues.entity'

class IssueDTO extends ItemDTO {
  public nombre: string;
  public incidencia_prioridad_id: number
  public incidencia_tipo_accion_id: number
  public restriccion_id: number
  public grupo_incidencia_id: number
  public subgrupo_incidencia_id: number
  public destinatario_st: number
  public descripcion: string
  public territorio_id: number
  public map_id: string
  public municipio_id: number
  public gestor_id: number
  public estado_incidencia_id: number
  public created_date: Date
  public created_user: string
  public last_edited_user: string
  public last_edited_date: Date
  public utmx: number
  public utmy: number
  public longitud: number
  public latitud: number
  public layer_id: number

  fromModel(model: Issue) {
    super.fromModel(model)
    this.nombre = model.nombre;
    this.incidencia_prioridad_id = model.incidencia_prioridad_id
    this.incidencia_tipo_accion_id = model.incidencia_tipo_accion_id
    this.restriccion_id = model.restriccion_id
    this.grupo_incidencia_id = model.grupo_incidencia_id
    this.subgrupo_incidencia_id = model.subgrupo_incidencia_id
    this.destinatario_st = model.destinatario_st
    this.descripcion = model.descripcion
    this.territorio_id = model.territorio_id
    this.map_id = model.map_id
    this.municipio_id = model.municipio_id
    this.gestor_id = model.gestor_id
    this.estado_incidencia_id = model.estado_incidencia_id
    this.created_date = model.created_date
    this.created_user = model.created_user
    this.last_edited_user = model.last_edited_user
    this.last_edited_date = model.last_edited_date
    this.utmx = model.utmx
    this.utmy = model.utmy
    this.longitud = model.longitud
    this.latitud = model.latitud
    this.layer_id = model.layer_id
  }
}

export default IssueDTO

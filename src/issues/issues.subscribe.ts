import { TokenMessage } from 'firebase-admin/lib/messaging/messaging-api'
import Logger from '../logger'
import SubscribeController from '../pubsub/suscribecontroller.template'
import Issues from './issues.entity'

class IssuesSubscribeController extends SubscribeController<Issues> {
  constructor(channel: string) {
    super(channel, 'incidencias', Issues)
  }

  generateMessage(_payload: Issues): TokenMessage {
    Logger.info(_payload)
    let territorio_modified = _payload.territorio_nombre
    if (territorio_modified.includes("Parque")) {
      const words = territorio_modified.split(' ');
      // Encuentra la última palabra
      let lastWord = words[words.length - 1];
      switch (lastWord) {
        case "Anaga":
          territorio_modified = lastWord; // Actualiza territorio_modified con el valor de lastWord
          break;
        case "Teno":
          territorio_modified = lastWord; // Actualiza territorio_modified con el valor de lastWord
          break;
        case "Teide":
          territorio_modified = lastWord; // Actualiza territorio_modified con el valor de lastWord
          break;
        default:
          break;
      }
    }
    const message: TokenMessage = {
      notification: {
        title: `Incidencia ${_payload.codigo} en ${territorio_modified}`,
        body: `${_payload.layer_name} Prioridad ${_payload.incidencia_prioridad_nombre} por ${_payload.last_edited_user}`,
      },
      android: {
        notification: {
          sound: 'default',
          clickAction: 'FCM_PLUGIN_ACTIVITY',
          icon: 'fcm_push_icon',
        },
      },
      data: {
        globalid: _payload.globalid,
        type: this.type,
      },
      token: '',
    }
    return message
  }
}

export default IssuesSubscribeController

import { Entity, PrimaryColumn, Column } from 'typeorm'

@Entity()
// eslint-disable-next-line @typescript-eslint/no-unused-vars
class Layer {
  @PrimaryColumn()
  public id: number

  @Column()
  public tablename: string

  @Column()
  public nombre: string

  @Column()
  public fk_name: string
}

export default Layer

import firebase from 'firebase-admin'
import {
  Message,
  TokenMessage,
} from 'firebase-admin/lib/messaging/messaging-api'
import Device from '../devices/devices.entity'
import DeviceService from '../devices/devices.service'
import Logger from '../logger'
import serviceAccount from '../serviceAccount.json'

class NotificationService {
  private firebase: firebase.app.App
  private static instance: NotificationService
  private deviceService = DeviceService.getInstance()

  private constructor() {
    this.firebase = firebase.initializeApp({
      credential: firebase.credential.cert(
        serviceAccount as firebase.ServiceAccount
      ),
    })
  }

  static getInstance(): NotificationService {
    if (!NotificationService.instance) {
      NotificationService.instance = new NotificationService()
    }

    return NotificationService.instance
  }

  pushNotification = async (payload: TokenMessage, userIds: string[]) => {
    try {
      //Aqui iran los devices en función de los valores del issue recibido
      const devices = await this.deviceService.findDevicesByUsers(userIds)
      if (devices) {
        this.sendNotification(payload, devices)
      }
    } catch (err) {
      Logger.error(err)
    }
  }

  sendNotification(payload: TokenMessage, devices: Device[]) {
    devices.forEach((device) => {
      this.firebase
        .messaging()
        .send(this.setTokenMessage(payload, device.token))
        .then(Logger.info)
        .catch(Logger.error)
    })
  }

  setTokenMessage(payload: TokenMessage, token: string): Message {
    payload.token = token
    return payload
  }
}

export default NotificationService

import { Column, Entity, OneToMany, ManyToMany, JoinTable, PrimaryColumn,} from 'typeorm'
import Device from '../devices/devices.entity'
import Group from '../groups/groups.entity'
import GroupTemplate from '../groupstemplate/groupstemplate.entity'

@Entity()
class User {
  @PrimaryColumn('character varying')
  public id: string

  @Column()
  public username: string

  @OneToMany(() => Device, (device) => device.user)
  devices: Device[]

  @ManyToMany(() => Group)
  @JoinTable()
  groups: Group[]

  @ManyToMany(() => GroupTemplate)
  @JoinTable()
  groupstemplate: GroupTemplate[]
}

export default User

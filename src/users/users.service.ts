import { getRepository } from 'typeorm'
import User from './users.entity'

class UserService {
  private static instance: UserService
  private userRepository = getRepository(User)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}

  static getInstance(): UserService {
    if (!UserService.instance) {
      UserService.instance = new UserService()
    }

    return UserService.instance
  }

  findUserById(id: string): Promise<User> {
    return this.userRepository.findOne({ where: { id: id } })
  }

  createUser(user: User): Promise<User> {
    return this.userRepository.save(user)
  }
}

export default UserService

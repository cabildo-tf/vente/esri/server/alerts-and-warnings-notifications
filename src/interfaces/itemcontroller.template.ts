import { Router, Request, Response, NextFunction } from 'express'
import {
  FindManyOptions,
  FindOneOptions,
  getManager,
  Raw,
  Repository,
} from 'typeorm'
import { QueryBuilder } from 'typeorm-express-query-builder'
import { ConfigProfile } from 'typeorm-express-query-builder/profile/config-profile'
import DBErrorException from '../exceptions/DBErrorException'
import NotFoundException from '../exceptions/ItemNotFoundException'
import Logger from '../logger'
import AuthorizationMiddleware from '../middleware/authorization.middleware'
import ItemProfile from '../utils/item.profile'
import ItemsProfile from '../utils/items.profile'
import Controller from './controller.interface'
import ItemDTO from './item-dto.interface'
import Item from './item.interface'

class ItemsController<T extends Item, D extends ItemDTO> implements Controller {
  public path: string
  public router = Router()
  private type: new () => T
  private typeDTO: new () => D
  private itemRepository: Repository<T>

  constructor(path: string, type: new () => T, typeDTO: new () => D) {
    this.path = path
    this.type = type
    this.typeDTO = typeDTO
    this.itemRepository = getManager().getRepository(this.type)
    this.intializeRoutes()
  }

  private intializeRoutes() {
    this.router.get(
      `${this.path}`,
      AuthorizationMiddleware,
      this.getAllItemsFromUser
    )
    this.router.get(
      `${this.path}/:globalid`,
      AuthorizationMiddleware,
      this.getItemByGlobalID
    )
  }

  getAllItemsFromUser = async (
    request: Request,
    response: Response,
    next: NextFunction
  ) => {
    const query = this.makeQuery(request, ItemsProfile)

    try {
      const items: T[] = await this.itemRepository.find(query)
      const itemsDTO: D[] = items.map((item) => {
        return this.mapper(item)
      })
      response.send(itemsDTO)
    } catch (err) {
      Logger.error(err)
      next(new DBErrorException())
    }
  }

  getItemByGlobalID = async (
    request: Request,
    response: Response,
    next: NextFunction
  ) => {
    const globalid = request.params.globalid
    request.query['globalid'] = globalid
    const query = this.makeQuery(request, ItemProfile) as FindOneOptions
    try {
      const item: T = await this.itemRepository.findOne(query)
      if (item) {
        response.send(this.mapper(item))
      }
    } catch (err) {
      Logger.error(err)
      next(new DBErrorException())
    }
    next(new NotFoundException(globalid))
  }

  makeQuery(request: Request, profile: ConfigProfile): FindManyOptions {
    const builder = new QueryBuilder(request.query, profile)

    const query = builder.build() as FindOneOptions
    if (!query?.where) {
      query.where = {}
    }
    query.where['userIds'] = Raw((alias) => `:user_id = ANY(${alias})`, {
      user_id: request?.user.id,
    })

    return query
  }

  mapper(model: T): D {
    const item = new this.typeDTO()
    item.fromModel(model)

    return item
  }
}

export default ItemsController

import Item from './item.interface'

abstract class ItemDTO {
  public globalid: string
  public codigo: string

  fromModel(model: Item) {
    this.globalid = model.globalid
    this.codigo = model.codigo
  }
}

export default ItemDTO

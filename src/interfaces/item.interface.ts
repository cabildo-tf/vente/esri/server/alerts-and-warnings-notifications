import { ViewColumn } from 'typeorm'

abstract class Item {
  @ViewColumn()
  public globalid: string

  @ViewColumn()
  public codigo: string

  @ViewColumn()
  public groupIds: string[]

  @ViewColumn()
  public userIds: string[]
}

export default Item

import HttpException from './HttpException'

class NotFoundException extends HttpException {
  constructor() {
    super(500, `Error in database`)
  }
}

export default NotFoundException

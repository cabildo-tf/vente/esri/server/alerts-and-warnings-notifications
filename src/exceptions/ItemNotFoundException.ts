import HttpException from './HttpException'

class NotFoundException extends HttpException {
  constructor(uuid: string) {
    super(404, `Item with uuid ${uuid} not found`)
  }
}

export default NotFoundException

import HttpException from './HttpException'

class TokenNotFoundException extends HttpException {
  status: number
  message: string
}

export default TokenNotFoundException

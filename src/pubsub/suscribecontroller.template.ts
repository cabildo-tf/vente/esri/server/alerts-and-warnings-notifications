import Subscribe from '../interfaces/subscribe.interface'
import Item from '../interfaces/item.interface'
import NotificationService from '../notifications/notification.service'
import { getManager, Repository } from 'typeorm'
import Logger from '../logger'
import { TokenMessage } from 'firebase-admin/lib/messaging/messaging-api'

abstract class SubscribeController<T extends Item> implements Subscribe {
  channel: string
  type: string
  private itemRepository: Repository<T>
  private notificationService = NotificationService.getInstance()

  constructor(channel: string, type: string, private typeClass: new () => T) {
    this.channel = channel
    this.type = type
    this.typeClass = typeClass
    this.itemRepository = getManager().getRepository(this.typeClass)
  }

  async listening(payload: Item) {
    const globalid = payload.globalid
    try {
      const item: T = await this.itemRepository.findOne({
        where: { globalid: globalid },
        //comprobar notificacion a 1
        //order by date (coger la ultima)
        //agrupar por nuevo campo (Vendra ya agrupado de la vista por el over partition)
      })
      if (item) {
        this.notificationService.pushNotification(
          this.generateMessage(item),
          item.userIds
        )
      }
    } catch (err) {
      Logger.error(err)
    }
  }

  abstract generateMessage(_payload: T): TokenMessage
}

export default SubscribeController

import { PgPubSub } from '@imqueue/pg-pubsub'
import SubscribeController from './suscribecontroller.template'
import Item from '../interfaces/item.interface'
import Logger from '../logger'
import config from '../ormconfig'
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions'

class PubSubService {
  private pgPubSub: PgPubSub
  private subscribes: SubscribeController<Item>[]

  constructor(subscribes: SubscribeController<Item>[]) {
    const dbconfig = config as PostgresConnectionOptions
    const host = dbconfig.host
    const user = dbconfig.username
    const password = dbconfig.password
    const port = dbconfig.port
    const dbname = dbconfig.database

    const connectionString = `postgres://${user}:${password}@${host}:${port}/${dbname}`

    this.pgPubSub = new PgPubSub({
      connectionString: connectionString,
      singleListener: false,
    })

    this.subscribes = subscribes
    this.intializePubSubs()
    this.pgPubSub
      .connect()
      .catch((err) => Logger.error('Connection error:', err))
  }

  private intializePubSubs() {
    this.subscribes.forEach((pgSubscribe) => {
      this.pgPubSub.listen(pgSubscribe.channel)
      this.pgPubSub.channels.on(pgSubscribe.channel, function (payload) {
        pgSubscribe.listening(payload as unknown as Item)
      })
    })
  }
}

export default PubSubService

FROM node:17-alpine3.12

COPY . /app
WORKDIR /app

# hadolint ignore=DL3018
RUN apk add --no-cache python3 && \
	apk add --no-cache --virtual .build-deps \
			make \
			g++ && \
	npm install && \
	apk del .build-deps && \
	chown -R node:node /app

USER node

CMD ["npx", "ts-node", "src/main.ts"]
# Alerts and warnings notifications

# Deploy
Es necesario crear un secret con la configuración de firebase, para ello hay que subir el fichero de
cuenta de google al servidor y a continuación:

```bash
docker secret create firebase-service-account ./serviceAccount.json
```

También es necesario crear el esquema app, así como las siguientes funciones en el postgres:
```sql
CREATE SCHEMA app
    AUTHORIZATION vente;

CREATE OR REPLACE FUNCTION app.incidencias_by_type(
	tablename text,
	fk_name text)
    RETURNS TABLE(globalid character varying, codigo character varying, nombre character varying, incidencia_prioridad_id smallint, incidencia_tipo_accion_id smallint, grupo_incidencia_id smallint, subgrupo_incidencia_id smallint, restriccion_id smallint, descripcion character varying, territorio_id smallint, territorio_nombre character varying, map_id character varying, municipio_id smallint, gestor_id smallint, estado_incidencia_id smallint, created_date timestamp without time zone, created_user character varying, last_edited_date timestamp without time zone,,utmx double precision, utmy double precision, longitud double precision, latitud double precision, "groupIds" integer[]) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
DECLARE
    query_     text;
BEGIN
  RETURN QUERY
    EXECUTE 'SELECT i.globalid, i.codigo, i.nombre,
               i.incidencia_prioridad_id, i.incidencia_tipo_accion_id,
               i.grupo_incidencia_id, i.subgrupo_incidencia_id, i.destinatario_st,
               i.restriccion as restriccion_id,
               i.descripcion, i.territorio_id, m.name as territorio_nombre,
               m.id as map_id, i.municipio_id,
               i.gestor_id, i.estado_incidencia_id,
               i.created_date, i.created_user, 
               ST_X(ST_Centroid(ST_Transform(i.shape, 32628))) as utmx,
               ST_Y(ST_Centroid(ST_Transform(i.shape, 32628))) as utmy,
               ST_X(ST_Centroid(ST_Transform(i.shape, 4326))) as latitud,
               ST_Y(ST_Centroid(ST_Transform(i.shape, 4326))) as longitud,
               app.array_distinct(ARRAY[i.destinatario_st, i.destinatario_ut, i.notificar_cecopin])::integer[] as groupIds
             FROM vente.incidencias i
             INNER JOIN vente.' || quote_ident(tablename) || ' a ON i.' || quote_ident(fk_name) || ' = a.globalid
             INNER JOIN app.map m ON i.territorio_id = m.territorio_id
             INNER JOIN (
                SELECT i.globalid, MAX(i.gdb_archive_oid) gdb_archive_oid
                FROM vente.incidencias i
                GROUP BY globalid
            ) b ON i.globalid = b.globalid AND i.gdb_archive_oid = b.gdb_archive_oid
            WHERE i.gdb_deleted_by IS NULL
            ORDER BY i.gdb_archive_oid DESC;';
END
$BODY$;


CREATE OR REPLACE FUNCTION app.array_distinct(anyarray) RETURNS anyarray AS $f$
  SELECT array_agg(DISTINCT x) FROM unnest($1) t(x);
$f$ LANGUAGE SQL IMMUTABLE;


CREATE OR REPLACE FUNCTION app.get_domain_values(
	domainname text)
    RETURNS TABLE(code integer, name character varying)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 100

AS $BODY$
BEGIN
  RETURN QUERY SELECT xml.code, xml.name
			FROM (SELECT definition
				  FROM sde.gdb_items g
				  WHERE g.name = domainname) d,
     			XMLTABLE ('/GPCodedValueDomain2/CodedValues/CodedValue' PASSING definition
         		COLUMNS
              			name character varying PATH 'Name',
              			code integer PATH 'Code'
      			) xml;
END
$BODY$;

```

Una vez arrancado el backend, desde la app se podrán logear los usuarios contra ArcGIS Enterprise, y
automáticamente quedarán registrado en el servicio de notificaciones y alertas en el grupo de "Todos".
Es necesario crear los grupos en la BDD y asignar los usuarios a los mismos.

También es necesario rellenar la tabla "layer" con la siguiente información:
```sql
INSERT INTO app.layer(id, tablename, nombre, fk_name)
VALUES
    (0, 'area_descanso', 'Área de descanso', 'area_descanso_id'),
    (1, 'arqueta', 'Arqueta', 'arqueta_id'),
    (2, 'baliza', 'Baliza', 'baliza_id'),
    (3, 'barrera', 'Barrera', 'barrera_id'),
    (4, 'cartel', 'Cartel', 'cartel_id'),
    (5, 'deposito', 'Depósito', 'deposito_id'),
    (6, 'dotacion_p', 'Dotación', 'dotacion_p_id'),
    (8, 'instalacion_recreativa', 'Instalación recreativa', 'instalacion_recreativa_id'),
    (9, 'marca', 'Marca', 'marca_id'),
    (10, 'mesa', 'Mesa', 'mesa_id'),
    (11, 'panel', 'Panel', 'panel_id'),
    (12, 'direccional', 'Direccional', 'direccional_id'),
    (13, 'zona_aparcamiento', 'Zona de aparcamiento', 'zona_aparcamiento_id'),
    (16, 'grupo_instalacion_recreativa', 'Grupo de instalación recreativa', 'grupo_instala_recreativa_id');
```
Rellenar la tabla "mapid" con las correspondecias entre el id del territorio y el mapa de collector usado
para mostrar las incidencias para ese territorio:
```sql
INSERT INTO app.mapid (id, name, map_id)
VALUES
    (1,'Este','40e152e37f714f1b8f923c3664c6d01b'),
    (2,'Centro','44d8bea22f8c416db09e3620dd296b91'),
    (3,'Oeste','e58574b2299d4bfaa214caa3916bd686'),
    (4,'Norte','440c8ce48cb74be6b8865e9ad596f006'),
    (5,'Sur','49d29e2cadc34c5aad1dc51292b5d23d'),
    (6,'Parque Nacional del Teide','1e8eaee2944048958d6c9e90a3f05dbf'),
    (7,'Parque Rural de Anaga','9c474a4e9e5740b187b34a9a9690f066'),
    (8,'Parque Rural de Teno','936a9f9826ab49228b35cb2a7696157a');
```

```sql
CREATE OR REPLACE FUNCTION vente.notify_new_issue()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    countIncidencias integer;
    countIncidenciasGlId integer;
BEGIN
    SELECT COUNT(*) INTO countIncidenciasGlId FROM vente.incidencias WHERE uuid = new.uuid AND globalid = new.globalid;
    SELECT COUNT(*) INTO countIncidencias FROM vente.incidencias WHERE uuid = new.uuid;
    IF ( countIncidenciasGlId > 1 ) THEN
       PERFORM pg_notify('new-issue', to_json(NEW.*)::text);
       RETURN NEW;
    ELSIF (countIncidencias = 1)  THEN
       PERFORM pg_notify('new-issue', to_json(NEW.*)::text);
    END IF;
    RETURN NULL;
   END;
$BODY$;

CREATE TRIGGER notify_new_issue_trigger
    AFTER INSERT
    ON vente.incidencias
    FOR EACH ROW
    WHEN (new.gdb_deleted_by IS NULL)
    EXECUTE PROCEDURE vente.notify_new_issue();

CREATE OR REPLACE FUNCTION notify_new_alert() RETURNS TRIGGER AS $$
   BEGIN
      PERFORM pg_notify('new-alert', to_json(NEW.*)::text);
      RETURN NEW;
   END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER notify_new_alert_trigger AFTER INSERT ON vente.alerta
FOR EACH ROW EXECUTE PROCEDURE notify_new_alert();

```

Esta tabla guarda los identificadores de las capas en el servicio para la creación de las incidencias.
[https://vente-pre.tenerife.es/server/rest/services/Incidencias/EdicionIncidencias_edit/FeatureServer](https://vente-pre.tenerife.es/server/rest/services/Incidencias/EdicionIncidencias_edit/FeatureServer)


# Endpoints

| Endpoint | Método | Descripción                                                                   |
|----------|--------|-------------------------------------------------------------------------------|
| /device  | POST   | Suscripción a las notificaciones:<br>{<br> 'token': '\<token del fcm\>'<br>}  |
| /alerts  | GET    | Listado de alertas                                                            |
| /issues  | GET    | Listado de incidencias                                                        |